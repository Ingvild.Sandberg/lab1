package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    /*
     * One round of rock paper scissors. 
     */
    public void run() { 
    	System.out.println("Let's play round " + roundCounter);
    	
    	//gets the human input and stores it in a variable 
    	String humanInput;
        humanInput = readInput("Your choice (Rock/Paper/Scissors)?");
        
        //makes sure the human input is either rock, paper or scissors. If not, the user must change their answer  
        while (!rpsChoices.contains(humanInput)) {
        	System.out.println("I do not understand " + humanInput + ". Could you try again?");
        	humanInput = readInput("Your choice (Rock/Paper/Scissors)?");
        }
        
        //gets the computers answer from the method computerAnswer()
        String compInput = computerAnswer(); 
        
        //announces who won the round 
        if (compInput.equals(humanInput)) { //if human and computer chose the same answer
        	System.out.println("Human chose " + humanInput + ", computer chose " + compInput + ". It's a tie!");
        } else { //if human and computer chose different answers 
        	String theWinner = whoWon(humanInput, compInput);
        	System.out.println("Human chose " + humanInput + ", computer chose " + compInput + ". " + theWinner + " wins!");
        }
        
        //announces the score so far 
        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
        
        //asks human if they want to continue playing and stores the answer in a variable 
        String play; 
        play = readInput("Do you wish to continue playing? (y/n)?");
        
        //checks if human wants to continue playing using the variable. If yes, the method calls itself and a new round begins
        if (play.equals("n")) {
        	System.out.println("Bye bye :)");
        } else if (play.equals("y")){
        	roundCounter++; 
        	run();
        }
    }
    
    /*
     * Generates an answer/input for the computer. 
     * The answer is either rock, paper or scissors, and is chosen randomly
     */
    public String computerAnswer() {
    	Random rand = new Random();
    	int randomIndex = rand.nextInt(3);
    	return rpsChoices.get(randomIndex); 
    }
    
    /*
     * Given the human input and the computer input, checks who the winner is, 
     * gives them a point and returns winner as a string.  
     */
    public String whoWon(String humanAnswer, String compAnswer) {
    	String winner = "yo"; //can be human or computer 
    	 
    	if (humanAnswer.equals("rock")) {
    		if (compAnswer.equals("scissors")) {
    			winner = "human"; 
    		} else {
    			winner = "computer";
    		}
    	}
    	if (humanAnswer.equals("paper")) {
    		if (compAnswer.equals("rock")) {
    			winner = "human"; 
    		} else {
    			winner = "computer";
    		}
    	}
    	if (humanAnswer.equals("scissors")) {
    		if (compAnswer.equals("rock")) {
    			winner = "computer";
    		} else {
    			winner = "human";
    		}
    	}
    	
    	if (winner == "human") {
    		humanScore++; 
    	} else {
    		computerScore++; 
    	}
    	
    	return winner;
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
